import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:widget_library/utils/app_localization.dart';

///Helper to create plural texts
String plural(int value,
    {String one, String two, String many, String few, String other}) {
  if (value != null) {
    if (value % 10 == 1) return one ?? other;
    if (value % 10 == 2) {
      return two ??
          Intl.plural(value, one: one, two: two, few: few, other: other);
    } else if (value == 3) {
      return few ??
          Intl.plural(value, one: one, two: two, few: few, other: other);
    } else {
      return Intl.plural(value, one: one, two: two, few: few, other: other);
    }
  } else {
    return "";
  }
}

/// Display dismissible Scaffold message
void showScaffoldMessage({
  @required String message,
  @required GlobalKey<ScaffoldState> key,
  @required BuildContext context,
  @required TextStyle textStyle,
  VoidCallback callback,
}) {
  key.currentState.showSnackBar(SnackBar(
    duration: Duration(seconds: 5),
    content: Text(
      message,
      style: textStyle,
    ),
    action: SnackBarAction(
      label: AppLocalizations?.of(context)?.translate("dismiss") ?? "Dismiss",
      onPressed: () {
        if (callback != null) {
          callback();
        }
        key.currentState.hideCurrentSnackBar();
      },
    ),
  ));
}
