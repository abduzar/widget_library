import 'package:flutter/widgets.dart';

class RelativeSize {
  double height;
  double width;
}

class SizeRuler {
  static BuildContext _context;
  static final SizeRuler _sizeRuler = SizeRuler._internal();

  SizeRuler._internal();

  factory SizeRuler(BuildContext context) {
    _context = context;
    return _sizeRuler;
  }

  static SizeRuler get instance {
    return _sizeRuler;
  }

  double get _height {
    return MediaQuery.of(_context).size.height;
  }

  double get _width {
    return MediaQuery.of(_context).size.width;
  }

  RelativeSize sizes(int percentage) {
    var size = RelativeSize();
    size.width = (_width * percentage) / 100;
    size.height = (_height * percentage) / 100;
    return size;
  }
}
