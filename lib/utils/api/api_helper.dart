import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:logger/logger.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

/// Helper for API calls (Dio library used [https://pub.dev/packages/dio]
abstract class ApiHelper {
  ///Constructor [baseUrl] should be provided
  ApiHelper({@required this.baseUrl});

  ///Url base
  final String baseUrl;

  ///Logger
  final Logger _logger =
      Logger(printer: PrettyPrinter(printEmojis: false, printTime: true));

  ///Dio object
  final Dio _dio = createDio();

  ///Initialize Dio
  static Dio createDio() {
    var dio = Dio();
    dio.interceptors.add(
      PrettyDioLogger(
          requestHeader: true,
          requestBody: true,
          responseBody: true,
          responseHeader: false,
          error: true,
          compact: true,
          maxWidth: 180),
    );
    dio.options.connectTimeout = 15000;
    dio.options.receiveTimeout = 30000;
    return dio;
  }

  ///Get token implementation
  Future<String> getToken();

  ///Manage not authenticated situation
  Future<void> manageToken();

  // ignore: public_member_api_docs
  Future<Response> execute(String url, HttpMethod method,
      // ignore: type_annotate_public_apis
      {data,
      bool authRequired = false,
      Map<String, Object> queryParameters,
      Options options}) async {
    var request = _buildRequest(
        method, url, authRequired, data, queryParameters, options);
    var result = await request;
    return result;
  }

  Future<Response> _buildRequest(
      HttpMethod method, String url, bool authRequired, data,
      [Map<String, Object> queryParameters, Options options]) async {
    Future<Response> req;
    switch (method) {
      case HttpMethod.get:
        req = _dio.get(url,
            options: options ?? await createOptions(needAuth: authRequired),
            queryParameters: queryParameters);
        break;
      case HttpMethod.post:
        req = _dio.post(url,
            data: data,
            options: options ?? await createOptions(needAuth: authRequired),
            queryParameters: queryParameters);
        break;
      case HttpMethod.patch:
        req = _dio.patch(url,
            data: data,
            options: options ?? await createOptions(needAuth: authRequired),
            queryParameters: queryParameters);
        break;
      case HttpMethod.delete:
        req = _dio.delete(url,
            data: data,
            options: options ?? await createOptions(needAuth: authRequired),
            queryParameters: queryParameters);
        break;
      case HttpMethod.put:
        req = _dio.put(url,
            data: data,
            options: options ?? await createOptions(needAuth: authRequired),
            queryParameters: queryParameters);
        break;
    }
    return req;
  }

  ///Create options for request
  Future<Options> createOptions(
      {@required bool needAuth, AuthType authType}) async {
    var headers = <String, dynamic>{};
    headers["Accept"] = "application/json";
    if (needAuth) {
      var token = await getToken();
      if (token == null || token == "") {
        throw DioError(error: "Authorization token required");
      } else {
        switch (authType) {
          case AuthType.bearerToken:
            _logger.d("Adding bearer token :: $token");
            headers["Authorization"] = "Bearer $token";
            break;
          case AuthType.accessToken:
            _logger.d("Adding access token :: $token");
            headers["Access_token"] = token;
        }
      }
    }
    var result = Options(headers: headers);
    return result;
  }
}

///Http methods
enum HttpMethod {
  ///Http method GET
  get,

  ///Http method POST
  post,

  ///Http method PATCH
  patch,

  ///Http method PUT
  put,

  ///Http method DELETE
  delete,
}

///Authentication types
enum AuthType {
  ///Authentication with bearer token
  bearerToken,

  ///Authentication with access token
  accessToken,
}
