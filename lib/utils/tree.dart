///Tree node example
class Node<T> {
  ///constructor
  Node(this.value, this.children);

  ///Node value
  final T value;

  ///Children of node
  final List<Node<T>> children;
}
