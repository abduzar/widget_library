import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

///Generic error message
const String genericErrorMessage = "Incorrect input ...";

///Generic class to extend , should be used to represent data structure
abstract class DataField {
  ///Constructor
  DataField({
    @required this.controller,
    @required this.field,
    this.focusNode,
    this.initialValue,
    this.hasError,
    this.readOnly = false,
  });

  ///Data field [TextEditingController] to access data
  TextEditingController controller;

  ///Data field [FocusNode]
  FocusNode focusNode;

  ///Data field [String] key value
  String field;

  ///Add if data may contain initial value
  String initialValue;

  ///Does field has errors [bool]
  bool hasError;

  ///Does field is readOnly [bool] by default it is [readOnly] is false
  bool readOnly;

  ///Data field [String] value to display
  String get title;
}

///Data model of server objects
abstract class ServerData {
  ///Text value getter to display or smth else
  String get text;

  ///Formatted text to display in type ahead suggestion list
  String get formatText;
}

///Widget to provide type ahead functionality
class TypeAheadFieldWidget extends StatefulWidget {
  ///Constructor
  TypeAheadFieldWidget({
    @required this.dataField,
    @required this.getList,
    this.textValidator = _textLengthValidator,
    this.filtered = false,
    this.prefixIcon,
    this.borderColor = Colors.red,
    this.errorMessage = genericErrorMessage,
    Key key,
  }) : super(key: key);

  ///Field object for current widget
  final DataField dataField;

  ///Desired prefix [Icon]
  final Widget prefixIcon;

  ///Desired text validator
  final bool Function(String) textValidator;

  ///List of suggestions got from server
  final Future<List<ServerData>> Function(String) getList;

  /// If list of suggestions needs to be filtered
  final bool filtered;

  /// Error message for validation
  final String errorMessage;

  /// Input broder [Color]
  final Color borderColor;


  @override
  TypeAheadFieldWidgetState createState() => TypeAheadFieldWidgetState();
}

///State object for [TypeAheadFieldWidget] to get selected object
class TypeAheadFieldWidgetState extends State<TypeAheadFieldWidget> {
  /// Which suggestion is currently selected
  ServerData selected;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 5.0),
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(color: widget.borderColor),
          bottom: BorderSide(color: widget.borderColor),
        ),
      ),
      child: TypeAheadFormField(
        initialValue: widget.dataField.initialValue,
        textFieldConfiguration: TextFieldConfiguration(
          controller: widget.dataField.controller,
          style: TextStyle(fontStyle: FontStyle.italic),
          decoration: InputDecoration(
            labelStyle: TextStyle(fontSize: 16),
            labelText: widget.dataField.title,
            prefixIcon: widget.prefixIcon,
            contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            border: InputBorder.none,
          ),
        ),
        suggestionsCallback: (suggestion) async {
          var list = await widget.getList(suggestion);
          if (widget.filtered) {
            var filtered = list
                .where((type) =>
                    type.text.toLowerCase().contains(suggestion.toLowerCase()))
                .toList();
            return filtered;
          } else {
            return list;
          }
        },
        itemBuilder: (context, suggestion) {
          var item = suggestion as ServerData;
          return ListTile(
            title: Text(item.formatText),
          );
        },
        validator: (value) {
          if (widget.textValidator(value)) {
            return null;
          } else {
            return widget.errorMessage;
          }
        },
        onSuggestionSelected: (suggestion) {
          var item = suggestion as ServerData;
          selected = item;
          setState(() {
            widget.dataField.controller.value =
                TextEditingValue(text: item.text);
          });
        },
      ),
    );
  }
}

bool _textLengthValidator(String text, {int textLength = 2}) {
  if (text.length < textLength) {
    return false;
  }
  return true;
}
