import 'package:flutter_test/flutter_test.dart';
import 'package:widget_library/utils/utils.dart';

void main() {
  test("Make it plural", () {
    var subscribers = plural(5,
        one: "подписчик",
        two: "подписчика",
        few: "подписчика",
        other: "подписчиков");
    expect(subscribers, "подписчиков");
  });
}
